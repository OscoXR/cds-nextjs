import React from 'react'

// MUI Component Imports
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Link from 'next/link';
import Typography from '@mui/material/Typography';

const WorldCard = ({world}) => {
  return (
    <Link href={"/world/" + world._id}>
      <Card 
      sx={{ minHeight: 260, maxWidth: 345 }}
      style= {{cursor: 'pointer' }}
      >
        <CardMedia
          component="img"
          height="250"
          image={world.ThumbUrl}
          alt={world.Title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {world.Title}
          </Typography>
          {/* <Typography variant="body2" color="text.secondary">
            {world.desc}
          </Typography> */}
        </CardContent>
      </Card>
    </Link>
  );
}

export default WorldCard