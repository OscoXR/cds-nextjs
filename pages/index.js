import { useState } from 'react';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import Head from 'next/head'
import Image from 'next/image';
import Link from 'next/link';
import Layout, { siteTitle } from '../components/layout'
// import { useContext } from 'react';
// import { UserContext } from '../lib/UserContext'
import TextField from '@mui/material/TextField';
import utilStyles from '../styles/utils.module.css'
// import LandingPageCarousel from '../components/LandingPageCarousel' // TODO
import { PAGES, SITE_ABOUT, SITE_TITLE } from '../lib/config';
import { VR_SAMPLE_ID, VR_SAMPLE_PROMPT, VR_URL } from '../lib/config';

export default function Home({}) {
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [status, setStatus] = useState(null); 

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      // Add User to Database if they do not exist
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/user/create", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
    }
    setLoading(false)
  };

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <center>
          <p>
            A recent college grad down on your luck, you're been applying to every open position on LunkedIn hoping for someone, anyone to give you a job.
            <br/>
            <br/>
            Just as you're about to give up, one company sends job offer! You become elated, but something seems off about this job posting...
            <br/>
            <br/>
            <Image
              src="/images/lunkedin.png"
              width={800}
              height={400}
              alt="Picture of the author"
            />
            <br/> 
            <br/>
            Deciding that you're worried over nothing, you accept the offer, pack your bags, and drive off to the remote academy to start your first day on the job.
            <br/> 
            <br/>
            What you find out soon enough is that this is not just an ordinary remote academy in the woods... this is an academy for cryptids to learn how to be human!
            <br/> 
            <br/>
            And your fellow professors are really attractive cryptids :) 
          </p>
        </center>
        <center>
          <Link href={"https://ep0.clown-math.university/"}>
            <h2>{">>>TRY OUT DEMO HERE<<<"}</h2>
            <Image
              src="https://blockade-platform-production.s3.amazonaws.com/thumbs/imagine/thumb_high_quality_detailed_digital_painting_cd_vr_computer_render_fantasy__7211741699df7f1b__4586320_.jpg?ver=1"
              width={500}
              height={250}
              alt="Picture of the author"
            />
            <br/>
            <Button variant="contained" color="secondary">Enter Demo</Button>
          </Link>
        </center>
        <center>
        <Link href={"https://newsletter.clown-math.university"}>
          <h3>{"Like our demo? Click here to join our mailing list to and get updates as we release new episodes!"}</h3>
        </Link>
        </center>
      </section>
    </Layout>
  )
}
