import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Image from 'next/image';
import Layout from '../../components/layout'
import Link from 'next/link';
import Head from 'next/head'
import styles from '../../components/layout.module.css'
import { useContext, useEffect, useState } from 'react'
import TextField from '@mui/material/TextField';
import { UserContext } from '../../lib/UserContext';


export default function World({ }) {
  // const router = useRouter()
  // const { id } = router.query
  const [id, setId] = useState(null);
  const [user] = useContext(UserContext);
  const [world, setWorld] = useState(null);

  useEffect(() => {
    (async function() {
      const id = window.location.href.split('/').pop(); // get last part of url lolol
      setId(id);
      fetch("/api/world/get?id="+id)
      .then(response => response.json())
      .then(data => {
        setWorld(data);
      });
  })();
}, [user]);


  return (
    <Layout>
      <Head>
        <title>View World</title>
      </Head>
      { world ?
      // Render Component With World Info
      <div>
        <center>
          <h1>{world.Title}</h1>
          {/* If Skybox Generation is Complete */}
          { (world.ThumbUrl !== "TBD") 
          ? 
          <div>
            <Link href={`https://boom-flicker-horn.glitch.me/index.html?id=${id}`}><Button variant="contained">Enter World</Button></Link>
            {' '}
            {/* // If logged in, check if you are owner */}
            {/* {(user.email === world.UserEmail) && <Link href={`remix/${id}`}><Button variant="contained">Edit World</Button></Link>} */}
            {/* // Anyone can remix! */}
            <Link href={`remix/${id}`}><Button variant="contained" color="secondary">Remix World</Button></Link>
            {' '}
            <Link href={`${world.ImageUrl}`}><Button variant="contained" color="success">Download World</Button></Link>
            <br/>
            <br/>
            <Image alt="world preview" height="336" width="672" src={world.ThumbUrl}/>
            <br/>
            <br/>
            <TextField
              disabled
              id="filled-disabled"
              label="Prompt"
              defaultValue={world.Prompt}
              variant="filled"
              fullWidth
              multiline
            />
            <br/>
            <br/>
            <TextField
              disabled
              id="filled-disabled"
              label="Style"
              defaultValue={world.SkyboxStyle}
              variant="filled"
              fullWidth
            />
          </div>  
          // If Skybox Generation is still processing
          :  <div><CircularProgress /><br/><br/><Alert severity="info">Manually refresh page in about 30 seconds if still loading</Alert><br/><br/></div>
          }
        </center>
      </div>
      // Still Loading World Info
      : <Alert severity="info">Loading World Info...</Alert>
      }
      <div className={styles.backToHome}>
        <Link href="/explore">← Back to Explore</Link>
      </div>
    </Layout>
  )
}
