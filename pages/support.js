import { FORM_URL, FEATURES_COMPLETE, FEATURES_UPCOMING } from '../lib/config'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link';
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Support({}) {
  // Allow this component to access our user state
  // const [user] = useContext(UserContext);
  const siteTitle = "Support"

  return (
    <Layout>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
      {/* <Link href={FORM_URL}>this form</Link> */}
      <center>
        <h2>{siteTitle}</h2>
        <Image
          priority
          src="/images/support.gif"
          height={250}
          width={250}
          alt={"support"}
        />
        <p>
          If you wanna say hi or yell at me for the app being broken, <br/>send me a tweet at <Link href={"https://twitter.com/OscoXR"}>@OscoXR</Link>!
        </p>
        <p>
          If you like the app, <Link href={"https://atemosta.com"}>check out my other stuff</Link> or <Link href={"https://ko-fi.com/atemosta"}>consider buying me a coffee {"<3"}</Link>
        </p>
      </center>
      </section>
    </Layout>
  )
}